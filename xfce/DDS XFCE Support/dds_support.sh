#!/bin/bash

REP_TRACES=/var/log
UID_ROOT=0     # Seuls les utilisateurs avec un $UID valant 0 ont les droits de root.
LIGNES=50      # Nombre de lignes sauvegardées par défaut.
E_XCD=66       # On ne peut pas changer de répertoire ?
E_NONROOT=67   # Code de sortie si non root.

# À exécuter en tant que root, évidemment.
if [ "$UID" -ne "$UID_ROOT" ]
then
  echo "Vous devez être root pour exécuter ce script."
  exit $E_NONROOT
fi  

if [ -n "$1" ]
# Teste la présence d'un argument (non vide) sur la ligne de commande.
then
  lignes=$1
else  
  lignes=$LIGNES # Par défaut, s'il n'est pas spécifié sur la ligne de commande.
fi  

#Ajout du paquet Gimp
echo "Ajout du module d'édition des fichiers .dds dans Gimp"
sudo apt install gimp-dds

#Emplacement + création de fichier
echo "Ajout du support du format .dds dans Thunar"
cd /usr/share/thumbnailers/
touch dds.thumbnailer

#On insère le contenu dans le fichier
echo "[Thumbnailer Entry]" >> dds.thumbnailer
echo "Version=1.0" >> dds.thumbnailer 
echo "Encoding=UTF-8" >> dds.thumbnailer 
echo "Type=X-Thumbnailer" >> dds.thumbnailer 
echo "Name=dds Thumbnailer" >> dds.thumbnailer
echo "MimeType=image/x-dds;" >> dds.thumbnailer
echo "Exec=/usr/bin/convert -thumbnail %s %i %o" >> dds.thumbnailer

cd ~/.local/share/mime/packages/
touch dds.xml

#On insère le contenu dans le fichier
echo "<?xml version="1.0" encoding="UTF-8"?>" >> dds.thumbnailer
echo "<mime-info xmlns="http://www.freedesktop.org/standards/shared-mime-info">" >> dds.thumbnailer 
echo '    <mime-type type="image/x-dds">' >> dds.thumbnailer 
echo "        <comment>dds file</comment>" >> dds.thumbnailer 
echo "        <icon name="image"/>" >> dds.thumbnailer
echo "        <glob-deleteall/>" >> dds.thumbnailer
echo "        <glob pattern="*.dds"/>" >> dds.thumbnailer
echo "    </mime-type>" >> dds.thumbnailer
echo "</mime-info>" >> dds.thumbnailer

#On met à jour pour la prise en compte du mimetype
update-mime-database ~/.local/share/mime