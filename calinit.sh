#!/bin/bash

#	Coloration syntaxique
ROUGE='\033[0;31m'  # Rouge
VERT='\033[0;32m' # Vert
RIEN='\033[0m' # Aucune couleur
BLINK='\033[5m' # Clignotement
E_NONROOT=67
UID_ROOT=0

#	Vérification du root
if [ "$UID" -ne "$UID_ROOT" ]
then
  echo "Vous devez être root pour exécuter ce script."
  exit $E_NONROOT
fi

#	Introduction
printf "${VERT}Bienvenue dans ce script de configuration post installation.${RIEN}\n"
echo "--------------------------------"

#	Mises à jour et ajouts des logiciels
notify-send -t 30 -i notification-message-IM "Mise à jour et ajouts des logiciels"
for (( ; ; )); do
  ping -q -c 1 https://www.calis-asso.org/ >/dev/null 2>&1 # Test de la connexion internet avant installation
  if [ $? -eq 0 ]; then
	printf "${ROUGE}${BLINK}Mise à jour de la liste des dépôts${RIEN}\n"
	sudo apt update
	printf "${ROUGE}${BLINK}Mise à jour des paquets${RIEN}\n"
	sudo apt upgrade -y
    sudo apt install neofetch wget git unzip vim pm-utils htop cherrytree lazpaint-qt5 simple-scan catfish galculator gcompris-qt inkscape gimp gimp-data-extras gimp-gmic blender gnucash keepassxc pdfmod audacious kdenlive vlc vlc-l10n unrar bleachbit redshift kdeconnect gajim deluge geany geany-plugins minetest gbrainy
    break
  fi
done

# Installer synaptic si celui-ci n'est pas trouvé
if ! [ -x "$(command -v synaptic)" ]; then # vérification du chemin de synaptic
    echo -e "Installation de wget \n\n"
    sudo apt-get install synaptic -y # installation de synaptic
    echo -e "\n\n"
fi

#	Introduction à la configuration de XFCE
echo "------------------------------------------"
echo "-------- Configuration de XFCE -----------"
echo "------------------------------------------"
notify-send -t 30 -i notification-message-IM "Configuration de XFCE en cours ..." # Affichage de la notification

#	Copie des différents éléments
cp /xfce/xfce4-panel.xml ~/.config/xfce4/xfconf/xfce-perchannel-xml/ # ajout des lanceurs au panel
cp /xfce/whiskermenu.rc ~/.config/xfce4/panel/ # Changement de la taille des icones du whisker-menu
cp /images/calis-logo-white.svg /usr/share/pixmaps/ # ajout du logo calis SVG
cp /images/calis-logo-white-32.png /usr/share/pixmaps/ # ajout du logo calis PNG 32px
cp /images/calis-background.png /usr/share/backgrounds/xfce/ # ajout du fond d'écran
cp /xfce/DDS XFCE Support/dds_support.sh ~/Téléchargements/ # copie du script du support DDS

#	Configuration via xconf
xfconf-query -c xfce4-power-manager -p /xfce4-power-manager/lock-screen-suspend-hibernate -s false # Desactiver la mise en veille et le verrouillage écran
xfconf-query -c xfce4-power-manager -p /xfce4-power-manager/dpms-enabled -s false #	Desactiver la mise en veille et le verrouillage écran
xfconf-query -c xfce4-power-manager -p /xfce4-power-manager/logind-handle-lid-switch -n -t bool -s false #	Fix du bug de l'écran laptop qui ne s'eteint pas lorsque l'écran est rabbatu
xfconf-query -c xfce4-screensaver -p /lock/enabled -s false # Desactiver la mise en veille et le verrouillage écran
xfconf-query -c xfce4-panel -p /panels/panel-0/size -s 35 #	Changement de la taille du panel
xfconf-query -c xfce4-desktop -p /desktop-icons/icon-size -s 64 #  Taille des icones du bureau
xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitor0/image-path -s /usr/share/backgrounds/xfce/calis-background.png # Modification du fond d'écran
xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitorDisplayPort-0/workspace0/last-image -s /usr/share/backgrounds/xfce/calis-background.png #	Modification du fond d'écran
xfconf-query -c xfwm4 -p /general/theme -s Adwaita-dark # Modification du thème
xfconf-query -c xsettings -p /Net/ThemeName -s Adwaita-dark #	Modification du thème
xfconf-query -c xsettings -p /Net/IconThemeName -s Adwaita # Modification du thème d'icônes
xfconf-query -c xsettings -p /Gtk/FontName -s Noto Sans 12 # Modification de la police
xfconf-query -c xsettings -p /Gtk/MonospaceFontName Monospace 14 # Modification de la police
xfconf-query -c xfce4-settings-manager -p /last/window-width -s 800 # Modification de la taille du gestionnaire de paramètres au lancement
xfconf-query -c xfce4-settings-manager -p /last/window-height -s 610 # Modification de la taille du gestionnaire de paramètres au lancement
xfconf-query -c thunar -p /last-separator-position -s 250 # Modification de la taille du gestionnaire de fichiers
xfconf-query -c thunar -p /last-window-width -s 900 # Modification de la taille du gestionnaire de fichiers
xfconf-query -c thunar -p /last-window-height -s 600 # Modification de la taille du gestionnaire de fichiers
xfconf-query --channel xfwm4 -p /general/popup_opacity -s 92 # Rendre les tooltips légèrement opaques afin de distinguer ce qui se trouve en dessous
xfconf-query --channel thunar --property /misc-compact-view-max-chars --create --type int --set 0 # Rétablir la vue complète des noms tronqués

#	Changer le comportement de XFCE Screenshooter
cd ~/Images
mkdir Screenshots
cd ~/.config/xfce4/
echo -e "app=env\nlast_user=calis\nlast_extension=jpg\nscreenshot_dir=file:/home/calis/Screenshots\nenable_imgur_upload=false\naction=1\ndelay=0\nregion=3\nshow_mouse=1\nshow_border=1\n" > xfce4-screenshooter # Inscription des paramètres dans le fichier config xfce4-screenshooter

#	Changer l'icône du menu whisker
cd ~/.config/gtk-3.0/
vi gtk.css +1$'i\n.xfce4-panel #whiskermenu-button-icon {background: @launcher_bg url("/usr/share/pixmaps/calis-logo-white-32.png") center; }' +wq # Insertion du CSS
xfce4-panel -r # Redémarrer le xfce panel
 
#	Creation du lanceur Calis
cd ~/.local/share/
mkdir applications
cd applications
touch calis.desktop
echo "[Desktop Entry]
Encoding=UTF-8
Name=Calis
GenericName=Asso
Comment=Site internet de l'association Calis
Exec=exo-open --launch WebBrowser https://calis-asso.org
Icon=/usr/share/pixmaps/calis-logo-white.svg
Terminal=false
Type=Application
Categories=Calis;" >> calis.desktop
chmod +x calis.desktop
cp ~/.local/share/applications/calis.desktop ~/Bureau

#	Ajout du raccourci pkill en pressant Ctrl + Suppr
cd ~/.config/xfce4/xfconf/xfce-perchannel-xml/
vi xfce4-keyboard-shortcuts.xml +59$'i\n      <property name="&lt;Primary&gt;&gt;Delete" type="string" value="pkill"/>' +wq

#	Configuration de Firefox
echo "------------------------------------------"
echo "-------- Configuration de Firefox --------"
echo "------------------------------------------"
notify-send -t 30 -i notification-message-IM "Configuration de Firefox"
firefox -silent -setDefaultBrowser # Firefox comme navigateur par défaut
firefox -no-remote -ProfileManager --createprofile calis # Créaton du nouveau profil firefox
cd ~/snap/firefox/common/.mozilla/firefox && PROFILCALIS=`basename *.calis` # On stocke le nom du profil généré aléatoirement dans une variable
cd $PROFILCALIS
rm -r * # On supprime le contenu du repertoire
unzip /applications/firefox/profile.zip -d ~/snap/firefox/common/.mozilla/firefox/$PROFILCALIS # Ajout du contenu du profil à firefox
firefox -no-remote -P calis # Lancement de firefox avec le profil calis

#	Controle parental par le fichier hosts
notify-send -t 30 -i notification-message-IM "Installation d'un controle parental par modification du fichier hosts"
echo "-----------------------------------"
echo "-------- Controle parental - hosts --------"
echo "-----------------------------------"

PS0='> ' 
LISTE=("[o] Oui" "[n] Non")
select CHOIX in "${LISTE[@]}" ; do
case $REPLY in
1|o)
cd ~ wget https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts # Recuperation du fichier hosts avec wget
cp -av /etc/hosts /etc/hosts.bak.$(date -d "yesterday" '+%Y-%m-%d') # Sauvegarde de l'ancien fichier hosts
mv hosts /etc/hosts # Remplacement du fichier hosts
break
;;

2|n)
echo "Entendu"
break
;;

esac
done

#	Support du DDS
notify-send "support du DDS"
echo "-----------------------------------"
echo "-------- Support du DDS -----------"
echo "-----------------------------------"

PS1='> ' 
LISTE=("[o] Oui" "[n] Non")
select CHOIX in "${LISTE[@]}" ; do
case $REPLY in
1|o)
cd ~/Téléchargements/
chmod +x dds_support.sh
./dds_support.sh
break
;;

2|n)
echo "Entendu"
break
;;

esac
done

#	Config des logs
cd /etc/systemd/
sed -i -e "s/\#SystemMaxFileSize=/\#SystemMaxFileSize=100M/g" journald.conf # limite du var/log pour gagner de l'espace disque
systemctl restart systemd-journald.service # reboot du service

#	Nettoyage
sudo apt autoremove
sudo journalctl --vacuum-size=100M
history -c

#	Redemarrage
xfce4-session-logout --reboot --fast





