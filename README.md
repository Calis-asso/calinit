# Calinit
Un script post installation à destination de la distribution Xubuntu.
Configure l'environnement utilisateur.

Auteur: Association CALIS - Adrien.V
Licence: WTFPL

## Lancer le script
- Ouvrir un terminal dans le dossier ou se trouve le script
- Tapez la commande ./calinit.short
- Suivez la procédure
