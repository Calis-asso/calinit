#!/bin/bash

echo -e "Récupération et Installation de Mblock Mlink"
cd /home/calis/
wget https://mblockapp.oss-cn-hongkong.aliyuncs.com/mblock5/linux/mLink-1.2.0-amd64.deb
sudo dpkg -i mLink-1.2.0-amd64.deb
sleep 1s
rm mLink-1.2.0-amd64.deb
echo -e "Création du lanceur mLink"
touch mlink.sh
chmod +x mlink.sh     
cat << EOF > mlink.sh
#!/bin/bash
ROUGE='\033[0;31m'  # Rouge
RIEN='\033[0m' # Aucune couleur
BLINK='\033[5m' # Clignotement
sudo xfce4-terminal -e  command mblock-mlink start
echo -e "${ROUGE}${BLINK}Ne pas fermer ce terminal si utilisation du mBot${RIEN}\n"
EOF
mkdir mblock
mv mlink.sh /home/calis/mblock/
