#!/bin/bash

#	Inkcut
sudo apt install python3-pip python3-pyqt5 python3-pyqt5.qtsvg libcups2-dev
pip3 install inkcut

#	Extensions Inkscape
echo -e "Extension inkscape"
wget https://github.com/inkstitch/inkstitch/releases/latest/download/inkstitch_2.1.2_amd64.deb
sudo dpkg -i inkstitch_2.1.2_amd64.deb
wget https://download.visicut.org/files/master/Debian-Ubuntu-Mint/visicut_1.9-141-ge75194f3-1_all.deb
sudo dpkg -i visicut_1.9-141-ge75194f3-1_all.deb
